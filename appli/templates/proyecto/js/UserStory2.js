function loadEvents() {
    //username de donde lo obtengo
    //   loadJSON(username);
}

function processJSON() {
    if (this.readyState == 4 && this.status == 200) {

        e = document.getElementById("fichajes");
        var obj = JSON.parse(this.responseText);
        var fichaje = obj.fichaje;
        for (let i = 0; i < fichaje.length; i++) {
            var fecha = fichaje[i].fecha;
            var tipo = fichaje[i].tipo;
            var altitud = fichaje[i].geolocalizacion[i].altitud;
            var latitud = fichaje[i].geolocalizacion[i].latitud;

            //Slice fecha 
            var dia = fecha.slice(0, 2);
            var mes = fecha.slice(3, 5);
            var año = fecha.slice(6, 10);
            var hora = fecha.slice(11, 13);
            var minutos = fecha.slice(14, 16);

            e.innerHTML += "<tr>" +
                "<td>" + tipo + "</td>" +
                "<td>" + dia + "</td>" +
                "<td>" + mes + "</td>" +
                "<td>" + año + "</td>" +
                "<td>" + hora + ":" + minutos + "</td>" +
                "<td>[" + altitud + "," + latitud + "]</td>" +
                "</tr>";
        }
    }
}
function loadJSON(username) {
    var jsonhttp = new XMLHttpRequest();
    jsonhttp.onreadystatechange = processJSON;
    jsonhttp.open("GET", "https://www.fichaje/" + username, true);
    jsonhttp.send();
}

