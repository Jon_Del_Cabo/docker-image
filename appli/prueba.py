from flask import Flask 
from flask import render_template

app = Flask(__name__)
 
@app.route('/')
def fichaje():
   return render_template("/proyecto/UserStory1.html")

@app.route('/historial')
def consulta():
   return render_template("/proyecto/UserStory2.html")

@app.route('/registrar')
def registrarse():
   return render_template("/proyecto/UserStory3.html")

if __name__ == "__main__":
   app.run(host="0.0.0.0", port="5000", debug= True)